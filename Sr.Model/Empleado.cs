﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sr.Interfaces;

namespace Sr.Model
{
    public class Empleado: IEmpleado
    {
        int _idEmpleado;

        public Empleado(int idEmpleado)
        {
            _idEmpleado = idEmpleado;
        }
        #region IEmpleado Members
        public int IdEmpleado { get { return _idEmpleado; } }
        public string Nombre { get; set; }
        public string Apellidos { get; set; }
        public float Salary { get; set; }
        #endregion
    }
}
