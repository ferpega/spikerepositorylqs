﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using Sr.Interfaces;

namespace Sr.Model
{
    public class Departamento : IDepartamento, INotifyPropertyChanged
    {
        int? _idDepartamento;
        List<IEmpleado> _empleados;
        private string _nombre;

        public Departamento(int? idDepartamento)
        {
            _idDepartamento = idDepartamento;
            _empleados = new List<IEmpleado>();
            NotificarCambio("IdDepartamento");
        }
        public Departamento() : this(null) { }

        //
        #region IDepartamento Members
        public int? IdDepartamento
        {
            get
            {
                return _idDepartamento;
            }
        }
        public string Nombre
        {
            get
            {
                return _nombre;
            }
            set
            {
                if (_nombre != value)
                {
                    _nombre = value;
                    NotificarCambio("Nombre");
                }
            }
        }
        public IEnumerable<IEmpleado> Empleados
        {
            get
            {
                return _empleados;
            }
        }
        public float MinSalary
        {
            get { return Empleados.Min(x => x.Salary); }
        }
        public float AvgSalary
        {
            get { return Empleados.Sum(x => x.Salary) / Empleados.Count(); }
        }
        public float MaxSalary
        {
            get { return Empleados.Max(x => x.Salary); }
        }
        public bool AddEmpleado(IEmpleado empleado)
        {
            bool result = false;
            try
            {
                _empleados.Add(empleado);
                result = true;
            }
            catch
            {
                throw;
            }
            return result;
        }
        #endregion

        #region INotifyPropertyChanged Members
        public event PropertyChangedEventHandler PropertyChanged;

        private void NotificarCambio(string nombrePropiedad)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(nombrePropiedad));
            }
        }
        #endregion
    }
}
