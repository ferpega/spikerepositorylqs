﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sr.PersistenceFake
{
    using System.Collections.Generic;
    using System.Linq;

    using Sr.Interfaces;
    using Sr.Model;

    public class RepoDepartamento : IRepoDepartamento
    {
        private readonly List<Departamento> innerList = new List<Departamento>
            {
                new Departamento(1) { Nombre = "Departamento1" },
                new Departamento(2) { Nombre = "Departamento2" },
                new Departamento(3) { Nombre = "Departamento3" },
                new Departamento(4) { Nombre = "Departamento4" },
            };

        public bool Save(IDepartamento departamento, out int idDepartamento)
        {
            if (this.innerList.Any(x => x.Nombre.Equals(departamento.Nombre, StringComparison.CurrentCultureIgnoreCase)))
            {
                idDepartamento = 0;
                return false;
            }

            var nextId = this.innerList.Max(x => x.IdDepartamento).Value + 1;
            this.innerList.Add(new Departamento(nextId) { Nombre = departamento.Nombre });

            idDepartamento = nextId;
            return true;
        }

        public IDepartamento GetById(int id)
        {
            return this.innerList.FirstOrDefault(x => x.IdDepartamento == id);
        }

        public IDepartamento GetByName(string name)
        {
            // Simulamos collation case insensitive
            return this.innerList.FirstOrDefault(x => x.Nombre.Equals(name, StringComparison.CurrentCultureIgnoreCase));
        }

        public int Count() { return innerList.Count(); }

        public void Dispose()
        {
            // Nada para hacer aquí
        }
    }
}
