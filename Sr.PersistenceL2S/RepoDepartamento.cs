﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Linq;
using Sr.Interfaces;

namespace Sr.PersistenceL2S
{
    public class RepoDepartamento : IRepoDepartamento
    {
        private readonly SrDataContext context;

        public RepoDepartamento()
        {
            context = new SrDataContext();
        }

        #region IRepoDepartamento Members
        public bool Save(IDepartamento departamento, out int idDepartamento)
        {
            if (DepartamentoAlreadyExists(departamento, out idDepartamento))
            {
                return false;
            }

            TableDepartamento tblDepartamento = new TableDepartamento();
            tblDepartamento.IdDepartamento = departamento.IdDepartamento;
            tblDepartamento.Nombre = departamento.Nombre;

            context.Departamentos.InsertOnSubmit(tblDepartamento);
            context.SubmitChanges();
            idDepartamento = tblDepartamento.IdDepartamento ?? -1;
            return true;

        }

        private bool DepartamentoAlreadyExists(IDepartamento departamento, out int idDepartamento)
        {
            idDepartamento = -1;
            var depto = GetByName(departamento.Nombre);
            if (depto != null) {
                idDepartamento = depto.IdDepartamento ?? idDepartamento;
                return true;
            } else {
                return false;
            }
        }

        public IDepartamento GetById(int id)
        {

            var result = (from d in context.Departamentos
                          where d.IdDepartamento == id
                          select d).FirstOrDefault();
            return result;

        }

        public IDepartamento GetByName(string name)
        {

            var result = (from d in context.Departamentos
                          where d.Nombre == name
                          select d).FirstOrDefault();
            return result;

        }

        public int Count()
        {
            return context.Departamentos.Count();
        }
        #endregion

        public void Dispose()
        {
            this.Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.context.Dispose();
            }
        }
    }
}
