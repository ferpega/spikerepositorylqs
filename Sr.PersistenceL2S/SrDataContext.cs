﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using System.Data.Linq;
using Sr.Interfaces;

namespace Sr.PersistenceL2S
{
    public class SrDataContext : DataContext
    {
        private const string connectionString = "Data Source='isostore:/Sr.sdf'";

        public SrDataContext()
            : base(connectionString)
        {
            CheckDatabaseExistence();
        }

        public Table<TableDepartamento> Departamentos
        {
            get
            {
                return this.GetTable<TableDepartamento>();
            }
        }

        private void CheckDatabaseExistence()
        {
            if (!DatabaseExists())
                CreateDatabase();
        }
    }
}
