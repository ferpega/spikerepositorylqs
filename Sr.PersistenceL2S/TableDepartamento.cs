﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;
using Sr.Interfaces;
using System.Data.Linq.Mapping;
using Microsoft.Phone.Data.Linq.Mapping;
using System.Collections.Generic;

namespace Sr.PersistenceL2S
{
    [Index(Columns="Nombre", IsUnique=true, Name="inx_dep_nombre")]
    [Table]
    public class TableDepartamento: IDepartamento
    {
        #region IDepartamento Members
        [Column(IsPrimaryKey = true, IsDbGenerated = true)]
        public int? IdDepartamento { get; set; }
        [Column(CanBeNull=false)]
        public string Nombre { get; set; }

        public IEnumerable<IEmpleado> Empleados
        {
            get { return null; }
        }

        public float MinSalary { get; set; }
        public float AvgSalary { get; set; }
        public float MaxSalary { get; set; }

        public bool AddEmpleado(IEmpleado empleado)
        {
            throw new NotImplementedException();
        }
        #endregion
    }
}
