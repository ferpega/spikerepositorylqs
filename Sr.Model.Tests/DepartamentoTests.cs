﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sr.Model.Tests
{
    using NUnit.Framework;

    [TestFixture]
    public class DepartamentoTests
    {
        [Test]
        public void Prueba()
        {
            var sut = new Departamento(1);

            Assert.That(sut.IdDepartamento, Is.EqualTo(1));
        }
    }
}
