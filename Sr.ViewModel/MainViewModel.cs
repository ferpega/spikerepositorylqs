namespace Sr.ViewModel
{
    using System;
    using System.Windows;
    using System.Windows.Controls;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using Sr.Interfaces;
    using Sr.Model;

    public class MainViewModel: ViewModelBase
    {
        private readonly IRepoDepartamento repoDepartamento;

        private string nombre;

        private string idBuscar;

        private string id;

        private string nombreBuscar;

        private int numeroDepartamentos;



        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel(IRepoDepartamento repoDepartamento)
        {
            if (repoDepartamento == null)
            {
                throw new ArgumentNullException("repoDepartamento");
            }

            this.repoDepartamento = repoDepartamento;
            this.LimpiarCommand = new RelayCommand(this.Limpiar);
            this.GoPageBCommand = new RelayCommand(this.GoPageB);
            this.GuardarCommand = new RelayCommand(this.Guardar);
            this.BuscarIdCommand = new RelayCommand(this.BuscarId, this.PuedeBuscarId);
            this.BuscarNombreCommand = new RelayCommand(this.BuscarNombre, this.PuedeBuscarNombre);
            this.NumeroDepartamentos = this.repoDepartamento.Count();
        }

        public string Id
        {
            get
            {
                return this.id;
            }
            set
            {
                this.id = value;
                this.RaisePropertyChanged(() => this.Id);
            }
        }

        public string Nombre
        {
            get
            {
                return this.nombre;
            }
            set
            {
                this.nombre = value;
                this.RaisePropertyChanged(() => this.Nombre);
            }
        }

        public string IdBuscar
        {
            get
            {
                return this.idBuscar;
            }
            set
            {
                this.idBuscar = value;
                this.RaisePropertyChanged(() => this.IdBuscar);
                this.BuscarIdCommand.RaiseCanExecuteChanged();
            }
        }

        public string NombreBuscar
        {
            get
            {
                return this.nombreBuscar;
            }
            set
            {
                this.nombreBuscar = value;
                this.RaisePropertyChanged(() => this.NombreBuscar);
                this.BuscarNombreCommand.RaiseCanExecuteChanged();
            }
        }
        
        public int NumeroDepartamentos 
        {
            get 
            {
                return numeroDepartamentos;
            }
            private set
            {
                numeroDepartamentos = value;
                this.RaisePropertyChanged(() => this.NumeroDepartamentos);
            }
        }


        public RelayCommand LimpiarCommand { get; private set; }

        public RelayCommand GuardarCommand { get; private set; }

        public RelayCommand BuscarIdCommand { get; private set; }

        public RelayCommand BuscarNombreCommand { get; private set; }

        public RelayCommand GoPageBCommand { get; private set; }

        

        public override void Cleanup()
        {
            //base.Cleanup();

            this.repoDepartamento.Dispose();
        }

        private void Limpiar()
        {
            this.Id = string.Empty;
            this.Nombre = string.Empty;
            this.IdBuscar = string.Empty;
            this.NombreBuscar = string.Empty;
        }

        private void GoPageB()
        {
            NavigationController nav = NavigationController.Instance;            
            nav.NavigateTo((Application.Current.RootVisual as INavigate), NavigationController.NavPageB);
        }

        private void Guardar()
        {
            int savedId;
            var departameto = new Departamento { Nombre = this.Nombre };
            var saveOk = this.repoDepartamento.Save(departameto, out savedId);

            if (saveOk)
            {
                this.NumeroDepartamentos = this.repoDepartamento.Count();
                MessageBox.Show(string.Format("Grabado: {0}", savedId));
            }
            else
            {
                MessageBox.Show(string.Format("El Departamento ya existe."));
            }
        }

        private void BuscarId()
        {
            IDepartamento dpto = this.repoDepartamento.GetById(Convert.ToInt32(this.IdBuscar));

            if (dpto != null)
            {
                this.Id = dpto.IdDepartamento.ToString();
                this.Nombre = dpto.Nombre;
            }
            else
            {
                MessageBox.Show(string.Format("No encontrado por ID:  {0}", this.IdBuscar));
            }
        }

        private bool PuedeBuscarId()
        {
            return TieneValor<int>(this.IdBuscar) && HayDepartamentosGrabados();
        }

        private void BuscarNombre()
        {
            IDepartamento dpto = this.repoDepartamento.GetByName(this.NombreBuscar);

            if (dpto != null)
            {
                this.Id = dpto.IdDepartamento.ToString();
                this.Nombre = dpto.Nombre;
            }
            else
            {
                MessageBox.Show(string.Format("No encontrado por ID:  {0}", this.NombreBuscar));
            }
        }

        private bool PuedeBuscarNombre()
        {
            return TieneValor<string>(this.NombreBuscar) && HayDepartamentosGrabados();
        }



        private bool TieneValor<T>(string valor)
        {
            if (typeof(T) == typeof(int)) {
                int idToFind;
                return !string.IsNullOrWhiteSpace(valor) && int.TryParse(this.IdBuscar, out idToFind);
            } else {
                return !string.IsNullOrWhiteSpace(valor);
            }
        }

        private bool HayDepartamentosGrabados()
        {
            return this.repoDepartamento.Count() > 0;
        }
    }
}