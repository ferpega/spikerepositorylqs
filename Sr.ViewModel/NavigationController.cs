﻿using System;
using System.Collections.Generic;
using System.Windows.Controls;
using System.Windows.Navigation;

namespace Sr.ViewModel
{
    public class NavigationController
    {
        static NavigationController instance;
        static Dictionary<string, Uri> registeredViews = new Dictionary<string, Uri>();
        Action<NavigationEventArgs> NavigationMethod;

        public static NavigationController Instance
        {
            get
            {
                if (instance == null) {
                    instance = new NavigationController();
                }
                return instance;
            }
        }

        private NavigationController()
        {
            registeredViews.Add(NavMainPage, new Uri(CreateUriWith(NavMainPage), UriKind.Relative));
            registeredViews.Add(NavPageB, new Uri(CreateUriWith(NavPageB), UriKind.Relative));
        }
        private static string CreateUriWith(string navigationPage)
        {
            return string.Format("/{0}." + NavXamlExtension, navigationPage);
        }



        public void NavigateTo(INavigate rootFrame, string navigationTarget)
        {
            //PhoneApplicationFrame rootFrame = App.Current.RootVisual as PhoneApplicationFrame;
            rootFrame.Navigate(registeredViews[navigationTarget]);
        }
        public void NavigateTo(INavigate rootFrame, string navigationTarget, string urlParams)
        {
            //PhoneApplicationFrame rootFrame = App.Current.RootVisual as PhoneApplicationFrame;
            string newUrl = string.Format(@"{0}?{1}", registeredViews[navigationTarget].ToString(), urlParams);

            rootFrame.Navigate(new Uri(newUrl, UriKind.RelativeOrAbsolute));
        }
        /*
        public void NavigateTo(string navigationTarget, Action<NavigationEventArgs> onNavigated)
        {
            PhoneApplicationFrame rootFrame = App.Current.RootVisual as PhoneApplicationFrame;
            NavigationMethod = onNavigated;
            rootFrame.Navigated += new NavigatedEventHandler(root_Navigated);
            rootFrame.Navigate(registeredViews[navigationTarget]);
        }*/



        public static string NavMainPage = "MainPage";
        public static string NavPageB = "PaginaB";
        private static string NavXamlExtension = "xaml";
    }
}
