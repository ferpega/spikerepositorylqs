﻿namespace Sr.ViewModel.Tests
{
    using NUnit.Framework;

    using Sr.PersistenceFake;

    [TestFixture]
    public class MainViewModelTests
    {
        [Test]
        public void CantFindByIdWithoutId()
        {
            var repository = new RepoDepartamento();
            var sut = new MainViewModel(repository);

            var result = sut.BuscarIdCommand.CanExecute(null);

            Assert.That(result, Is.False);
        }

        [Test]
        public void CantFindByIdIfIdIsNotNumber()
        {
            var repository = new RepoDepartamento();
            var sut = new MainViewModel(repository) { IdBuscar = "Hola" };

            var result = sut.BuscarIdCommand.CanExecute(null);

            Assert.That(result, Is.False);
        }

        [Test]
        public void CanFindByIdIfIdIsNumber()
        {
            var repository = new RepoDepartamento();
            var sut = new MainViewModel(repository) { IdBuscar = "1" };

            var result = sut.BuscarIdCommand.CanExecute(null);

            Assert.That(result, Is.True);
        }
    }
}
