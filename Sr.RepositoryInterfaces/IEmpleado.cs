﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sr.Interfaces
{
    public interface IEmpleado
    {
        int IdEmpleado { get; }
        string Nombre { get; set; }
        string Apellidos { get; set; }
        float Salary { get; set; }
    }
}
