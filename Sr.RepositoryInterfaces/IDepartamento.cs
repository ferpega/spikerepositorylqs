﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Sr.Interfaces
{
    public interface IDepartamento
    {
        int? IdDepartamento { get; }
        string Nombre { get; set; }
        IEnumerable<IEmpleado> Empleados { get; }
        float MinSalary { get; }
        float AvgSalary { get; }
        float MaxSalary { get; }

        bool AddEmpleado(IEmpleado empleado);
    }
}
