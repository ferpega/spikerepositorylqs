﻿using System;
using System.Net;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Shapes;

namespace Sr.Interfaces
{
    public interface IRepoDepartamento : IDisposable
    {
        bool Save(IDepartamento departamento, out int idDepartamento);
        IDepartamento GetById(int id);
        IDepartamento GetByName(string name);
        int Count();
    }
}
